#!/usr/bin/python3
# -*- coding: utf-8 -*-

import threading
import random
import sys
import socketserver
import time
import simplertp

PORT = 53001


def get_time():
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'


def get_arguments():
    # Comprobar formato argumentos terminal.
    # python3 serverrtp.py <IPServidorSIP>:<puertoServidorSIP> <servicio> <fichero>
    # python3 serverrtp.py 127.0.0.1:6001 heyjude cancion.mp3
    try:
        ip_servsip = sys.argv[1].split(':')[0]
        port_servsip = int(sys.argv[1].split(':')[1])
        servicio = sys.argv[2]
        file = sys.argv[3]

    except:
        sys.exit('Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service> <file>')

    return ip_servsip, port_servsip, servicio, file


class EchoHandler(socketserver.BaseRequestHandler):
    ip_servsip, port_servsip, servicio, file = get_arguments()

    def handle(self):
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8')
        petition = received.split()[0]

        # Histórico
        print(f'{get_time()} SIP from {self.client_address[0]}:{self.client_address[1]}: {data.decode("utf-8")}')
        if petition == 'INVITE':
            # Obtengo el puerto y la dirección IP a la que enviar el archivo mediante RTP.
            global client_ip
            global client_port
            client_ip = received.split('o=')[1].split(' ')[1].split('\r\n')[0]
            client_port = int(received.split('m=audio ')[1].split(' ')[0])

            # Envía respuesta
            response = 'SIP/2.0 200 OK\r\n\r\n'
            sock.sendto(response.encode('utf-8'), self.client_address)
            print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {response}')  # Histórico

        elif petition == 'ACK':
            # Envío de datos RTP.
            sender = simplertp.RTPSender(ip=client_ip, port=client_port, file=self.file, printout=True)
            sender.send_threaded()
            recv, address = sock.recvfrom(1024)
            if recv.decode('utf-8').split(' ')[0] == 'BYE':
                sender.finish()
                print(f'{get_time()} SIP from {address[0]}:{address[1]}: {recv.decode("utf-8")}')  # Histórico
                response = 'SIP/2.0 200 OK\r\n'
                sock.sendto(response.encode('utf-8'), self.client_address)
                print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {response}')


def main():
    ip_servsip, port_servsip, servicio, file = get_arguments()

    try:
        serv = socketserver.UDPServer(('', PORT), EchoHandler)
        print(f'{get_time()} Starting...')  # Histórico
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        # Envía REGISTER al iniciar el serverrtp
        message = f'REGISTER sip:{servicio}@songs.net SIP/2.0\r\n\r\n'
        serv.socket.sendto(message.encode('utf-8'), (ip_servsip, port_servsip))
    except:
        sys.exit('Error registrando serverrtp en serversip')

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == '__main__':
    main()
