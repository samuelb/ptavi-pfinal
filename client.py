#!/usr/bin/python3
# -*- coding: utf-8 -*-
# python3 client.py 127.0.0.1:6001 sip:yo@clientes.net sip:heyjude@songs.net 10 miaudio.mp3

import sys
import socket
import socketserver
import time
import threading


IP = '127.0.0.1'
PORT = 34543


def get_time():
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'


def get_arguments():
    # Comprobar formato argumentos terminal.
    # $ python3 client.py <IPServidorSIP>:<puertoServidorSIP> <dirCliente> <dirServidorRTP> <tiempo> <fichero>
    try:
        ip_servsip = sys.argv[1].split(':')[0]
        port_servsip = int(sys.argv[1].split(':')[1])
        sip_client = sys.argv[2]
        sip_servrtp = sys.argv[3]
        time = int(sys.argv[4])
        file = sys.argv[5]

    except:
        sys.exit('Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>')

    return ip_servsip, port_servsip, sip_client, sip_servrtp, time, file


class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""


    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido.

        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


def main():
    ip_servsip, port_servsip, sip_client, sip_servrtp, tiempo, file = get_arguments()

    # Comunicación con socket UDP.
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f'{get_time()} Starting...')  # Histórico

            # INVITE
            protocol = f'INVITE {sip_servrtp} SIP/2.0'
            content_type = 'Content-type: application/sdp'
            session_name = sip_servrtp.split("@")[0].split(":")[1]
            sdp_elements = f'v=0\r\no={sip_client} {IP}\r\ns={session_name}\r\nt={tiempo}\r\nm=audio {PORT} RTP'
            message = f'{protocol}\r\n{content_type}\r\n\r\n{sdp_elements}'
            my_socket.sendto(message.encode('utf-8'), (ip_servsip, port_servsip))
            print(f'{get_time()} SIP to {ip_servsip}:{port_servsip}: {protocol}')  # Histórico

            # Manejamos respuestas del servidor
            data, address = my_socket.recvfrom(1024)
            print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')  # Histórico

            # Enviamos respuesta si el mensaje del servidor es el siguiente.
            if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 302 Moved Temporarily':
                message = f'ACK {sip_servrtp} SIP/2.0\r\n\r\n'
                my_socket.sendto(message.encode('utf-8'), (ip_servsip, port_servsip))
                print(f'{get_time()} SIP to {ip_servsip}:{port_servsip}: {message}')  # Histórico

            # Bloque de envío al servidorrtp
                wanted_address = data.decode('utf-8').split('\r\n')[1].split()[1]
                wanted_address_ip = wanted_address.split('@')[1].split(':')[0]
                wanted_address_port = int(wanted_address.split('@')[1].split(':')[1])

                # Mensaje para servertp
                protocol = f'INVITE {wanted_address} SIP/2.0'
                content_type = 'Content-type: application/sdp'
                session_name = wanted_address.split("@")[0].split(":")[1]
                sdp_elements = f'v=0\r\no={sip_client} {IP}\r\ns={session_name}\r\nt={tiempo}\r\nm=audio {PORT} RTP'
                message = f'{protocol}\r\n{content_type}\r\n\r\n{sdp_elements}'
                my_socket.sendto(message.encode('utf-8'), (wanted_address_ip, wanted_address_port))
                print(f'{get_time()} SIP to {wanted_address_ip}:{wanted_address_port}: {protocol}')  # Histórico

            # Gestión de mensaje recibido de serverrtp
                data, address = my_socket.recvfrom(1024)
                print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')  # Histórico

                # Comprobamos mensaje recibido de serverrtp
                if data.decode('utf-8') == 'SIP/2.0 200 OK\r\n\r\n':
                    message = f'ACK {sip_servrtp.split("@")[0]}{address[0]}{address[1]} SIP/2.0\r\n\r\n'
                    my_socket.sendto(message.encode('utf-8'), (wanted_address_ip, wanted_address_port))
                    print(f'{get_time()} SIP to {wanted_address_ip}:{wanted_address_port}: {message}')  # Histórico

                # Se queda escuchando para recibir los paquetes RTP
                    # Abrimos un fichero para escribir los datos que se reciban
                    RTPHandler.open_output(file)
                    with socketserver.UDPServer((IP, PORT), RTPHandler) as serv:
                        print("Listening...")
                        # El bucle de recepción (serv_forever) va en un hilo aparte,
                        # para que se continue ejecutando este programa principal,
                        # y podamos interrumpir ese bucle más adelante
                        threading.Thread(target=serv.serve_forever).start()
                        # Paramos un rato. Igualmente, podríamos esperar a recibir BYE,
                        time.sleep(tiempo)
                        print("Time passed, shutting down receiver loop.")
                        # Paramos el bucle de recepción, con lo que terminará el thread,
                        # y dejamos de recibir paquetes
                        serv.shutdown()
                    # Cerramos el fichero donde estamos escribiendo los datos recibidos
                    RTPHandler.close_output()

                    # Enviamos BYE
                    message = f'BYE {sip_servrtp.split("@")[0]}@{address[0]}:{address[1]} SIP/2.0\r\n\r\n'
                    my_socket.sendto(message.encode('utf-8'), (wanted_address_ip, wanted_address_port))
                    print(f'{get_time()} SIP to {wanted_address_ip}:{wanted_address_port}: {message}')  # Histórico

                    # Esperando OK del serverRTP.
                    data, address = my_socket.recvfrom(1024)
                    print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')  # Histórico
                    # Recibimos OK y cierra cliente.
                    if data.decode('utf-8') == 'SIP/2.0 200 OK\r\n\r\n':
                        my_socket.close()

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == '__main__':
    main()
